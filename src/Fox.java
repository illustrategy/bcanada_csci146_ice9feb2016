/**
 * CONCRETE Fox class that implements
 * the speak() method of the abstract Animal superclass
 */

/**
 * @author bcanada
 *
 */
public class Fox extends Animal {

	@Override
	public void speak()
	{
		// What does the fox say?
		System.out.println("Ring-ding-ding-ding-dingeringeding!");
		
	} // end concrete method speak
	
} // end concrete class Fox
