/**
 * Test class to illustrate polymorphic behavior at runtime
 */

/**
 * @author bcanada
 *
 */
public class RuntimePolymorphismDemo {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		Animal[] animals = new Animal[4];
		
		animals[0] = new Dog();
		animals[1] = new Cat();
		animals[2] = new Mouse();
		animals[3] = new Fox();
		
		/*
		 * Now we will loop through the animals array,
		 * but note that NOWHERE in the for-loop do we
		 * EXPLICITLY refer to a dog, cat, mouse, or fox object!
		 */
		for ( Animal thisAnimal : animals )
		{
			// Polymorphically call the speak() method for the
			// specific type of animal -- the choice of speak()
			// method is determined at runtime (execution time),
			// NOT compile time! Thus, this is an example of
			// RUNTIME polymorphism.
			thisAnimal.speak();
			
			/*
			 * Note: In contrast, COMPILE-TIME polymorphism is
			 * just another way of saying "method overloading".
			 * In RUNTIME polymorphism, we're not over-LOADING
			 * methods -- rather, we are over-RIDING them!
			 */
			
		} // end for
		
	} // end main

} // end class
