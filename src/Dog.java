/**
 * CONCRETE Dog class that implements
 * the speak() method of the abstract Animal superclass
 */

/**
 * @author bcanada
 *
 */
public class Dog extends Animal {

	@Override
	public void speak()
	{
		// What does a dog say?
		System.out.println("Woof!");
		
	} // end concrete method speak
	
} // end concrete class dog
