/**
 * Class definition for an abstract Animal 
 * 
 * It's abstract because we want the animal
 * to speak (when commanded to do so), but
 * we don't know exactly what TYPE of animal
 * it will be, and so we don't know exactly
 * what it will say! 
 * 
 * The implementation of the speak() method
 * will be handled by various subclasses of Animal.
 * 
 * And because it is INCOMPLETE, an abstract class
 * CANNOT BE INSTANTIATED! DO NOT FORGET THIS... EVER!!!!!
 * 
 */

/**
 * @author bcanada
 * @version 19-Feb-2016
 */
public abstract class Animal {

	// Since the METHOD is declared abstract,
	// the containing CLASS must also be declared abstract!
	public abstract void speak();
	
} // end ABSTRACT class Animal
