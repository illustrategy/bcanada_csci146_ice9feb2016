/**
 * CONCRETE Cat class that implements
 * the speak() method of the abstract Animal superclass
 */

/**
 * @author bcanada
 *
 */
public class Cat extends Animal {

	@Override
	public void speak()
	{
		// What does a cat say?
		System.out.println("Meow!");
		
	} // end concrete method speak
	
} // end concrete class Cat
