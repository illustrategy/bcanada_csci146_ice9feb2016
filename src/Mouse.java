/**
 * CONCRETE Mouse class that implements
 * the speak() method of the abstract Animal superclass
 */

/**
 * @author bcanada
 *
 */
public class Mouse extends Animal {

	@Override
	public void speak()
	{
		// What does a mouse say?
		System.out.println("Squeak!");
		
	} // end concrete method speak
	
} // end concrete class Mouse
